package eu.tsoml.task1.model;

import java.util.Arrays;

public class BooksHolder {

    private Book[] books;

    public BooksHolder(Book[] books) {
        this.books = books;
    }

    public void coAuthorsCheck(String author) {

        for (Book book : books) {
            if (Arrays.asList(book.getAuthor()).contains(author) && book.getAuthor().length >= 2) {
                System.out.println(book);
            }
        }

    }

    public void threeAuthorsCheck() {

        for (Book book : books) {
            if (book.getAuthor().length >= 3) {
                System.out.println(book);
            }
        }
    }

}
