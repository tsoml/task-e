package eu.tsoml.task1.model;

import java.util.Arrays;

public class Book {

    private String title;
    private String[] author;
    private String publishingHouse;
    private int yearOfPublication;

    public Book(String title, String[] author, String publishingHouse, int yearOfPublication) {
        this.title = title;
        this.author = author;
        this.publishingHouse = publishingHouse;
        this.yearOfPublication = yearOfPublication;
    }


    @Override
    public String toString() {
        return "Book: " + title +
                ", by " + Arrays.toString(author) +
                ", " + publishingHouse +
                " " + yearOfPublication;
    }

    public String getTitle() {
        return title;
    }

    public String[] getAuthor() {
        return author;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }
}

