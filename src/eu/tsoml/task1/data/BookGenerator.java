package eu.tsoml.task1.data;

import eu.tsoml.task1.model.Book;

public class BookGenerator {

    public static final int BOOKS_NUMBER = 6;

    public static Book[] generate() {

        Book[] books = new Book[BOOKS_NUMBER];

        books[0] = new Book("The Last Wish", new String[]{"Andrzej Sapkowski"}, "Orbit", 1993);
        books[1] = new Book("Sword of Destiny", new String[]{"Andrzej Sapkowski"}, "Orbit", 1992);
        books[2] = new Book("The 10X Rule", new String[]{"Grant Cardone"}, "Gildan Media", 2011);
        books[3] = new Book("Before the Storm", new String[]{"Christie Golden"}, "Del Rey", 2018);
        books[4] = new Book("Good Omens", new String[]{"Terry Pratchett", "Neil Gaiman"}, "William Morrow", 2006);
        books[5] = new Book("Tales from the Shadowhunter Academy", new String[]{"Cassandra Clare", "Sarah Rees Brennan", "Maureen Johnson", "Robin Wasserman"}, "Margaret K. McElderry Books", 2016);

        return books;
    }
}
