package eu.tsoml.task1;

import eu.tsoml.task1.data.BookGenerator;
import eu.tsoml.task1.model.BooksHolder;

import java.util.Scanner;

public class Runner {

    public void run() {

        Scanner scan = new Scanner(System.in);

        BooksHolder booksHolder = new BooksHolder(BookGenerator.generate());

        System.out.println("Task 1.1: Co-Authors Check");
        System.out.println("Enter the author: ");
        String currentAuthor = scan.nextLine();
        booksHolder.coAuthorsCheck(currentAuthor);

        System.out.println("\nTask 1.2: More than three authors" );
        booksHolder.threeAuthorsCheck();


    }
}
