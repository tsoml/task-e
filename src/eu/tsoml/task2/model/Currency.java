package eu.tsoml.task2.model;


public class Currency {

    private String name;
    private double buyRate;
    private double sellRate;

    public Currency(String name, double buyRate, double sellRate) {
        this.name = name;
        this.buyRate = buyRate;
        this.sellRate = sellRate;
    }

    public String getName() {
        return name;
    }

    public double getBuyRate() {
        return buyRate;
    }

    public double getSellRate() {
        return sellRate;
    }

}