package eu.tsoml.task2.model;

public class BanksHolder {

    private Bank[] banks;

    public BanksHolder(Bank[] banks) {
        this.banks = banks;
    }


    public void calc(String bankName, String userCurrency, String bankCurrency, int amount) {
        for (Bank currentBank : banks) {
            if (currentBank.isAvailableBank(bankName)) {
                currentBank.convert(userCurrency, bankCurrency, amount);
            }
        }
    }

}
