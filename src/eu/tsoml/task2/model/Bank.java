package eu.tsoml.task2.model;

public class Bank {

    String name;
    Currency[] currencies;

    public Bank(String name, Currency[] currencies) {
        this.name = name;
        this.currencies = currencies;
    }

    public boolean isAvailableBank(String bankName) {
        return name.equalsIgnoreCase(bankName);
    }


    public void convert(String userCurrency, String bankCurrency, int amount) {


        if (userCurrency.equalsIgnoreCase("uah")) {
            for (Currency currency : currencies) {
                if (currency.getName().equalsIgnoreCase(bankCurrency)) {
                    System.out.println(String.format("You money in %s: %.2f", bankCurrency, amount / currency.getSellRate()));
                    System.out.println(currency);
                }
            }
        } else {
            for (Currency currency : currencies) {
                if (currency.getName().equalsIgnoreCase(userCurrency)) {
                    System.out.println(String.format("You money in %s: %.2f", bankCurrency, amount * currency.getBuyRate()));
                }
            }
        }
    }

}
