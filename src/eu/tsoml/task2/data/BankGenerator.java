package eu.tsoml.task2.data;

import eu.tsoml.task2.model.Bank;
import eu.tsoml.task2.model.Currency;

public class BankGenerator {

    public static final int BANKS_NUMBER = 3;

    public static Bank[] generate() {

        Bank[] banks = new Bank[BANKS_NUMBER];

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 28, 28.5);
            currencies[1] = new Currency("eur", 31, 31.5);
            currencies[2] = new Currency("gbr", 50, 50.5);
            banks[0] = new Bank("MonoBank", currencies);
        }

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 28, 28.5);
            currencies[1] = new Currency("eur", 31, 31.5);
            currencies[2] = new Currency("gbr", 50, 50.5);
            banks[1] = new Bank("PrivatBank", currencies);
        }


        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 28, 28.5);
            currencies[1] = new Currency("eur", 31, 31.5);
            currencies[2] = new Currency("gbr", 50, 50.5);
            banks[2] = new Bank("UniversalBank", currencies);
        }

        return banks;
    }

}
