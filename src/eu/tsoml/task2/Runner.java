package eu.tsoml.task2;


import eu.tsoml.task2.data.BankGenerator;
import eu.tsoml.task2.model.BanksHolder;

import java.util.Scanner;

public class Runner {

    public void run() {

        Scanner scan = new Scanner(System.in);

        BanksHolder banksHolder = new BanksHolder(BankGenerator.generate());

        System.out.println("Enter bank name: ");
        String bank = scan.nextLine();

        System.out.println("Enter your currency (UAH, USD, EUR or GBR): ");
        String userCurrency = scan.nextLine();

        System.out.println("Enter the amount of " + userCurrency + " that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());


        if (userCurrency.equalsIgnoreCase("uah")){
            System.out.println("Enter the currency to convert (USD,EUR or GBR): ");
            String bankCurrency = scan.nextLine();
            banksHolder.calc(bank, userCurrency, bankCurrency, amount);
        } else {
            banksHolder.calc(bank, userCurrency, "uah", amount);
        }

    }

}
